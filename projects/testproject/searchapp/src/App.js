import './App.css';
import JSONDATA from './dataset.json';
import {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useLocation
} from 'react-router-dom'



function App() {
  return (
    <Router>
    <Switch>
        <Route paht='/'>
          <Search/>
        </Route>
    </Switch>
    </Router>
    );
}

function useQuery() {
  return new URLSearchParams(useLocation().search)
}

function Search(){
  const[searchTerm, setSearchTerm] = useState('');
  const history = useHistory();
  let query = useQuery();

  return (
    <div className="App">
      
      <input 
        name='search' 
        type="text"
        value={query.get('search', "")}
        placeholder="Search" 
        onChange={event => {
          setSearchTerm(event.target.value);
          history.push("/?search=" + event.target.value)
          }}/>
      {JSONDATA.filter((val)=> {
        if (query.get('search')) {
          if (val.toLowerCase().includes(query.get('search').toLowerCase())) {
            return val
          }
        } else {
          if (searchTerm == "") {
            return val
          } else if (val.toLowerCase().includes(searchTerm.toLowerCase())) {
            return val
          }
        }
      }).map((val) => {
        return <div className="person" key={val}>
          <p>{val}</p>
          </div>
      })}
    </div>
  )
}

export default App;
